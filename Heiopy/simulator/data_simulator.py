import datetime
import numpy as np
from noise import pnoise1
import pandas as pd

from threading import Timer,Thread,Event

def test(var):
        print("test " + var)

class DataSimulation:
    def __init__(self):
        self.is_running = False
        self.plot = None
        self.df = pd.DataFrame() 
        self.current_ch = None
        self.timer = None #Timer(0.1, lambda: self.callback(np.random.randint(0,1023)))
        
    def select_channel(self,ch):
        self.current_ch = ch

    def callback(self,data):
        tmpFrame=pd.DataFrame({ 'ch_' + str(self.current_ch) : [data]})
        self.df=self.df.append(tmpFrame, ignore_index=True)  
        #print(data)
        try:
            self.plot.update(data)
        except:
            pass
        #self.timer = Timer(0.1, lambda: self.callback(np.random.randint(0,1023)))
        self.start()

    def setup(self, ch=0):
        self.current_ch = ch
        self.start()
        print("initalized")

    def start(self):
        #print("started")
        if self.current_ch == 0:
            self.timer = Timer(0.01, lambda: self.callback(np.random.randint(0,255))) # np.random.randint(0,1023)))
        elif self.current_ch >= 10:
            self.timer = Timer(0.01, lambda: self.callback(pnoise1(self.current_ch))) # np.random.randint(0,1023)))
        else:
            self.timer = Timer(0.01, lambda: self.callback(pnoise1(np.random.randint(0,255)*0.49))) # np.random.randint(0,1023)))
        self.timer.start()
        self.is_running = True

    def stop(self):
        if self.is_running:
            #print("stopped")
            self.timer.cancel()
            #self.timer = None
            self.is_running = False
    
    def start_data(self):
        self.start()

    def stop_data(self):
        self.stop()    

#
#def data_sim(update_function):
#    if gz.runSim:
#        #print(datetime.datetime.now())
#        update_function(1)
#        threading.Timer(numpy.random.randint(1,10), data_sim(update_function)).start()

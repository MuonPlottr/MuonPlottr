import pigpio
import time
import datetime
import numpy as np
#from Heiopy.plot import *
import pandas as pd

class MuonHunter:
    def __init__(self):
        self.is_running = False
        self.plot = None
        self.df = pd.DataFrame(data={'GM1' : np.nan, 'GM2' : np.nan,'Muons':np.nan}, index=(0,0,0)) 
        self.current_ch = 0
        gm1ticks = []
        gm2ticks = []
        self.gm1_last = 0
        self.gm2_last = 0
        self.GPIO = pigpio.pi()
        self.time_offset = self.GPIO.get_current_tick()
        self.GM1_cb = None
        self.GM2_cb = None

    def muon_detected(self,gpio, level, tick):
        #print(gpio, level, tick)
        timestr = time.strftime("%Y-%m-%d_%H-%M-%S")
        #start = time.time()
        #camera.capture("images/" + timestr +  ".jpg",use_video_port=True )
        #stop  = time.time()
        #print("new muon: " + timestr + ", took: ",  (stop-start),  " seconds")
        #print("new muon: " + timestr + ", tick: ",  tick)
        value=0
        if gpio == 18:
            # GM1
            value=1
            #if pigpio.tickDiff(self.gm2_last,tick) < 150:
                #print("muon")
            #    value=3
            #self.gm1_last=tick
            tmpFrame=pd.DataFrame({ 'GM' + str(value) : [tick/1000000]})
            self.df=self.df.append(tmpFrame, ignore_index=True)  
        else:
            # GM2
            value=2
            tmpFrame=pd.DataFrame({ 'GM' + str(value) : [tick/1000000]})
            self.df=self.df.append(tmpFrame, ignore_index=True)  
            #if pigpio.tickDiff(self.gm1_last,tick) < 150:
                #print("muon")
            #    value=3
            #self.gm2_last=tick
        if self.is_running:
            self.plot.update(value, pigpio.tickDiff(self.time_offset,tick))


    def setup(self):
        timestr = time.strftime("%Y-%m-%d %H:%M:%S")
        print("MuonHunter, current time: " + timestr)
        self.GPIO.set_mode(18, pigpio.INPUT) # GM1 count
        self.GPIO.set_mode(17, pigpio.INPUT) # GM2 count

        
    def start(self):
        #print("started")
        self.is_running = True
        self.time_offset = self.GPIO.get_current_tick()
        self.df = pd.DataFrame(data={'GM1' : np.nan, 'GM2' : np.nan,'Muons':np.nan}, index=(0,0,0)) 
        self.GM1_cb = self.GPIO.callback(17, pigpio.FALLING_EDGE, self.muon_detected)
        self.GM2_cb = self.GPIO.callback(18, pigpio.FALLING_EDGE, self.muon_detected)
        
    def stop(self):
        self.is_running = False
        self.GM1_cb.cancel()
        self.GM2_cb.cancel()
        self.plot.clear()
    
    def start_data(self):
        self.start()

    def stop_data(self):
        self.stop()    
#import os
#import numpy as np
#import pandas as pd
import bokeh.plotting as bp
#import bokeh.client as bc
#import bokeh.embed as be
#import bokeh.charts as bh
#import bokeh.models as bm
import bokeh.layouts as bl
from bokeh.io import output_notebook
#import ipywidgets as widgets
#from IPython.display import display
from collections import deque
from bokeh.io import push_notebook
import time
#from bokeh.models import Range1d

from bokeh.layouts import widgetbox
from bokeh.models import CustomJS
from bokeh.models.widgets import RadioButtonGroup
from bokeh.models.widgets import Button, Toggle, Select
#import bokeh.settings 
from bokeh.resources import INLINE
#import Heiopy.arduino.firmata as firmata
from bokeh.models import ColumnDataSource
from bokeh.models import FuncTickFormatter
from bokeh.models import FixedTicker
from bokeh.models.widgets import Slider
from bokeh.models import Legend
import pandas as pd

js_code="""
        // Define a callback to capture errors on the Python side
        function callback(msg){
            console.log("Python callback returned unexpected message:", msg)
        }
        callbacks = {iopub: {output: callback}};

        // Generate a command to execute in Python
        //var ranges = {x0: x_range.attributes.start,
        //              y0: y_range.attributes.start,
        //              x1: x_range.attributes.end,
        //               y1: y_range.attributes.end}
        var value = cb_obj.get('%s')
        var value_str = JSON.stringify(value)
        var cmd = "%s(" + value_str + ")"
        //var cmd = "ss(value)"

        // Execute the command on the Python kernel
        var kernel = IPython.notebook.kernel;
        kernel.execute(cmd, callbacks, {silent : false});
"""

class LivePlot:
    cb1 = CustomJS(code=js_code % ('label','lp.toggle_button_clicked'))
    cb2 = CustomJS(code=js_code % ('value','lp.channel_cb'))
    cb3 = CustomJS(code=js_code % ('value','lp.slider_cb'))

    def __init__(self, device):
        #self.lv_t = deque([0])
        #self.lv_k = deque([0])
        self.t0 = time.time() 
        self.coincidence_time = 350

        self.source1 = ColumnDataSource(data=dict(x0=[], y0=[], x1=[], y1=[]))        
        self.source2 = ColumnDataSource(dict(x0=[], y0=[], x1=[], y1=[]) )        
        
        self.device = device
        #output_notebook(resources=INLINE)
        #output_notebook()

        self.plot = bp.figure(title= "live plot", plot_width =700, plot_height=350, toolbar_location="above", y_range=(0,3))
        
        p1 = self.plot.segment(x0='x0', y0='y0', x1='x1', y1='y1', color="#F4A582",line_width=2,line_alpha=0.5,legend="GM counts", source=self.source1)
        p2 = self.plot.segment(x0='x0', y0='y0', x1='x1', y1='y1', color="navy",line_width=1, line_alpha=0.3, legend="Muons",source=self.source2)
        self.plot.legend.location = "top_left"
        self.plot.xgrid.grid_line_color = None
        self.plot.ygrid.grid_line_color = None #minor_
        self.plot.yaxis.ticker=FixedTicker(ticks=[1,2,3])
        self.plot.yaxis.formatter = FuncTickFormatter(code="""
             function (tick) {
                 var name_dict = { "1": "GM1",
                                   "2": "GM2",
                                   "3":"Muon" }; 
                 return name_dict[tick]}; """)

        self.plot.xaxis.formatter = FuncTickFormatter(code="""
             function (tick) {return tick/1000000}; """)

        #self.plot.legend.location=(50,60)
        self.plot.legend.orientation="horizontal"

        
        #self.plot.circle(x='x', y='y', color="red", legend="signal", source=self.source)
        
        #fig.y_range = Range1d(0, 1.5)
        #fig2.x_range = Range1d(0, 100)
        #fig2.y_range = Range1d(0, 1.5)
        #fig.yaxis[0].ticker=bm.FixedTicker(ticks=[0,1])
        #fig2.yaxis[0].ticker=bm.FixedTicker(ticks=[0,1])
        #bp.show(bl.column(fig,fig1, fig2))
        self.gui = self.create_gui()
        bp.show(bl.row(self.plot, self.gui))
        #print("plot ready")
        
    def toggle_button_clicked(self,b):
        #global startDAQ
        if self.device.is_running:
            self.device.stop_data()
            print("starting")
        else:
            self.source1.data = dict(x0=[], y0=[], x1=[], y1=[])  
            self.source2.data = dict(x0=[], y0=[], x1=[], y1=[])   
            self.device.start_data() 
            print("stopping")
        #print("button clicked %s" % b)

#    def on_start_button_clicked(self,b):
#        self.startDAQ = True
#        print("start")
#        self.device.start_DAQ()
#        
#    def on_stop_button_clicked(self,b):
#        self.startDAQ = False
#        print("stop")
#        self.device.stop_DAQ()
            
    def channel_cb(self,pin):
        #print("channel cb %s" % pin)
        self.device.select_channel(int(pin))
        
    def slider_cb(self,value):
        #print("channel cb %s" % value)
        self.coincidence_time = int(value)
        x = self.find_muons(self.source1.data)
        if len(x):
            muons = dict(x0=x, y0=[0]*len(x), x1=x, y1=[3]*len(x))
            #print(muons)
            #self.source2.stream(muons,  rollover=300)
            self.source2.data = muons
            push_notebook()


    def create_gui(self):
        # FIXME: hardcoding instance names here (lp.)!
        buttonStart = Toggle(label="Start/Stop",  button_type="primary", callback=self.cb1)
        #buttonStart.on_click(self.on_start_button_clicked)
        #buttonStop = Button(label="Stop",  button_type="warning", callback=self.callback)
        #buttonStop.on_click(self.on_stop_button_clicked)
        #radio_button_group = RadioButtonGroup(labels=["pin 0", "pin 1", "pin 2"], active=0, callback=self.cb2)
        selector = Select(title="select pin", options=["0", "1", "2"],value=str(self.device.current_ch), callback=self.cb2)
        #radio_button_group.on_click(self.channel_cb)
        slider = Slider(start=5, end=600, value=350, step=1, title="coincidence time [us]", callback=self.cb3)
        return widgetbox(buttonStart, selector, slider, width=150)
    
    def find_muons(self, data):
        gm1_last =0 
        gm2_last =0
        x = []
        for key, values in data.items():
            #print("key %s values %s" % (key,values))
            if key == 'x1':
                for i,tick in enumerate(values):
                    muon = False
                    if data['y1'][i] == 1:
                        # GM1
                        if (tick - gm2_last) < self.coincidence_time:
                            #print("muon found")
                            muon = True
                        gm1_last=tick
                    elif data['y1'][i] == 2:
                        # GM2
                        if (tick - gm1_last) < self.coincidence_time:
                            muon = True
                            #print("muon found")
                        gm2_last=tick
                    if muon:
                        x.append(tick)
                        if self.device.df["Muons"].last_valid_index() == None or (self.device.df["Muons"][self.device.df["Muons"].last_valid_index()] < tick/1000000):
                            tmpFrame=pd.DataFrame({ 'Muons' : [tick/1000000]})
                            self.device.df=self.device.df.append(tmpFrame, ignore_index=True)  
        return x
   

    def update(self, value, time=None, channel=None):
        #for i in range(10000):
        #if ((max(self.lv_t)-min(self.lv_t)) > 10):
        #self.lv_t.popleft()
        #self.lv_k.popleft()
        if time is None:
            t = time.time()-self.t0
        else:
            t = time
        #rv = np.random.randint(20)
        #if (rv == 1):
        #self.lv_t.append(t)
        #self.lv_k.append(value) #was rv
        #x, y = self.source.data['x'][-1], self.source.data['y'][-1]
        # construct the new values for all columns, and pass to stream
        # for points:
        #new_data = dict(x=[t], y=[value])
        # for horizontal lines
        if value == 1:
            #GM1
            new_data = dict(x0=[t], y0=[0], x1=[t], y1=[value])
        elif value == 2:
            #GM2
            new_data = dict(x0=[t], y0=[1], x1=[t], y1=[value])
        #push_notebook()
        #print("before")
        # find muons
        x = self.find_muons(self.source1.data)
        if len(x):
            muons = dict(x0=x, y0=[0]*len(x), x1=x, y1=[3]*len(x))
            #print(muons)
            #self.source2.stream(muons,  rollover=300)
            self.source2.data = muons
        self.source1.stream(new_data, rollover=600)   
        push_notebook()

        
    def clear(self):
         pass
        

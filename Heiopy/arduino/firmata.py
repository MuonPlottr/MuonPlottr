from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

#from Heiopy.plot import plot
from ipywidgets import interact
import ipywidgets as widgets


import asyncio
import pandas as pd
#from threading import Thread
#from multiprocessing import Process
#from IPython.kernel.zmq.eventloops import register_integration
from ipykernel.eventloops import register_integration 

##github.com/jupyter/ngcm-tutorial/blob/master/Day-1/IPython%20Kernel/Background%20Jobs.ipynb
#from IPython.lib import backgroundjobs as bg
#
#jobs = bg.BackgroundJobManager()

loop = None
# FIXME: needs cleanup!
# https://github.com/ipython/ipykernel/blob/197a4a81c38dfcafb1cddb3029329137c8748b23/ipykernel/eventloops.py
# code from here: https://github.com/ipython/ipykernel/issues/21
@register_integration('asyncio')
def loop_asyncio(kernel):
    '''Start a kernel with asyncio event loop support.'''
    global loop    
    loop = asyncio.get_event_loop()

    def kernel_handler():
        loop.call_soon(kernel.do_one_iteration)
        loop.call_later(kernel._poll_interval, kernel_handler)

    loop.call_soon(kernel_handler)
    #try:
    #if not loop.is_running():
    loop.run_forever()
            #loop.until_complete(asyncio.ensure_future(board_loop()))           
    #finally:
    #    loop.close()

class DataAcquistion:
    def __init__(self):
        self.board = PyMata3(0)
        self.current_ch = 0
        global loop
        self.df=pd.DataFrame() 
        self.plot = None
        self.current_ch = None
        self.is_running = False
        
    def select_channel(self,ch):
        self.current_ch = ch


    def callback(self,data):
        if data[0] == self.current_ch:
            val = data[1]
            tmpFrame=pd.DataFrame({ 'ch_' + str(self.current_ch) : [val]})
            self.df=self.df.append(tmpFrame, ignore_index=True)  
            #print(val)
            self.plot.update(val)


    def setup(self,ch=0):
        self.current_ch = ch
        #interact(self.select_channel, pin=widgets.IntSlider(min=0,max=2,step=1,value=pin))
        self.board.set_sampling_interval(10)
        self.start()
        print("initalized")

    def start(self):
        self.board.set_pin_mode(0, Constants.ANALOG, self.callback)# Constants.CB_TYPE_ASYNCIO)
        self.board.set_pin_mode(1, Constants.ANALOG, self.callback)# Constants.CB_TYPE_ASYNCIO)
        self.board.set_pin_mode(2, Constants.ANALOG, self.callback)# Constants.CB_TYPE_ASYNCIO)
        self.is_running = True

    def stop(self):
        self.board.disable_analog_reporting(0)
        self.board.disable_analog_reporting(1)
        self.board.disable_analog_reporting(2)
        #board.set_pin_mode(0, Constants.OUTPUT)
        #board.set_pin_mode(1, Constants.OUTPUT)
        #board.set_pin_mode(2, Constants.OUTPUT)
    
    def start_data(self):
        #print("start_DAQ")
        global loop
        if not loop.is_running():
            loop.run_forever()
            self.is_running = True

    def stop_data(self):
        #print("stop_DAQ")
        global loop
        loop.stop()
        self.is_running = False
            

async def board_loop():
    while True:
        pass
        #       await asyncio.sleep(1)
        #board.sleep(0.11000)


def acquire():
    pass
#    global jobs
#    jobs.new('run_loop()')
#    jobs.status()
    #global t
    #t = Thread(target=run_loop)
    #t.start()

async def shutdown():
    board.shutdown()

    
def reset():
    board.send_reset()

#if __name__ == "__main__":
#   setup()
    #while True:
    #    loop()
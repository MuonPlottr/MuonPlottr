from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

import time

BOARD_LED = 13

TRIG_PIN = 2
ECHO_PIN = 4

timeSet = 0
timeDown = 0
distance = 0

board = PyMata3()

def measure():
    board.digital_write(BOARD_LED, 0)
    board.digital_write(TRIG_PIN, 0)
    board.sleep(0.000002)
    board.digital_write(TRIG_PIN, 1)
    board.digital_write(BOARD_LED, 1)
    board.sleep(0.000010)
    board.digital_write(TRIG_PIN, 0)
    board.digital_write(BOARD_LED, 0)

def echoCallback(data):
    global timeSet
    global timeDown
    global distance
    if data[1] == 1:
        timeSet = time.time()
    if data[1] == 0:
        timeDown = time.time()
        distance = (timeDown - timeSet) / 0.000058138
        if distance < 50:
            print(distance)

def setup():
    board.set_pin_mode(BOARD_LED, Constants.OUTPUT)
    board.set_pin_mode(TRIG_PIN, Constants.OUTPUT)
    board.set_pin_mode(ECHO_PIN, Constants.INPUT, echoCallback)

def loop():
    measure()
    board.sleep(0.01)

if __name__ == "__main__":
    setup()
    while True:
        loop()
from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

POT_PIN = 0;

board = PyMata3()
    
def setup():
    board.set_pin_mode(POT_PIN, Constants.ANALOG)
    print("All working.")

def loop():
    val = board.analog_read(POT_PIN)
    print(val)

if __name__ == "__main__":
    setup()
    while True:
        loop()
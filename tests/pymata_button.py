from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants

BOARD_LED = 13
INPUT_PIN = 8

flag = 0
counter = 0

board = PyMata3()
    
def setup():
    board.set_pin_mode(BOARD_LED, Constants.OUTPUT)
    board.set_pin_mode(INPUT_PIN, Constants.INPUT)
    print("All working. Press the button")

def loop():
    global flag
    global counter
    val = board.digital_read(INPUT_PIN)
    global flag
    if val == 1 and flag == 0:
        flag = 1
        counter = counter + 1
        board.digital_write(BOARD_LED, 1)
        print(counter)
    elif flag == 1 and val == 0:
        board.digital_write(BOARD_LED, 0)
        flag = 0


if __name__ == "__main__":
    setup()
    while True:
        loop()
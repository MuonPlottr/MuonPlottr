#!/bin/sh

set -e

if [ "$(id -u)" -ne 0 ]; then
    echo "Error: script has to be executed with superuser priviledges."
    exit 1
fi

echo "[*] Installing basic packages ..."
apt-get install ansible git

echo "[*] Cloning MuonPlottr from CERN GitLab ..."
git clone https://gitlab.cern.ch/MuonPlottr/MuonPlottr.git /root/MuonPlottr

cd /root/MuonPlottr/ansible
echo localhost >> hosts

echo "[*] Running ansible ..."
ansible-playbook -c local -i hosts main.yml

echo "[*] Complete!"
